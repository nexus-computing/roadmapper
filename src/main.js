import { createApp } from 'vue'
import App from './App.vue'
import mitt from 'mitt' // to use Event Bus with Vue.js 3

const emitter = mitt()
const app = createApp(App)
app.config.globalProperties.emitter = emitter
app.mount('#app')
