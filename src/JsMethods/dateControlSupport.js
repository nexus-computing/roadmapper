
/*
  WARNING
  new Date("2015-11-25"); => Wed Nov 25 2015 ## I choose this format per default
  new Date(2015, 11, 25); => Fri Dec 25 2015 ## I choose this format only for getNumberOfWeeksInMonth()
*/

export default {
  getPeriodInMonth (startD, endD) {
    const d1 = new Date(startD)
    const d2 = new Date(endD)
    const d1Y = d1.getFullYear()
    const d2Y = d2.getFullYear()
    const d1M = d1.getMonth()
    const d2M = d2.getMonth()

    return d2M + 12 * d2Y - (d1M + 12 * d1Y) + 1
  },
  getNumberOfTheWeekInYear (dtString) {
    const dt = new Date(dtString)
    var tdt = new Date(dt.valueOf())
    var dayn = (dt.getDay() + 6) % 7
    tdt.setDate(tdt.getDate() - dayn + 3)
    var firstThursday = tdt.valueOf()
    tdt.setMonth(0, 1)
    if (tdt.getDay() !== 4) {
      tdt.setMonth(0, 1 + ((4 - tdt.getDay() + 7) % 7))
    }
    return 1 + Math.ceil((firstThursday - tdt) / 604800000)
  },
  getNumberOfWeeksInYear (y) {
    // isLeap = (Année bissextile / Schaltjahr 366d instead of 365d)
    const d = new Date(`${y}-1-1`) // first day of selected year
    const isLeap = new Date(`${y}-2-29`).getMonth() === 1 // in Date month[1] = Februar

    // check for a Jan 1 that's a Thursday OR a leap year that has a Wednesday jan 1. Otherwise it's 52 weeks
    return d.getDay() === 4 || (isLeap && d.getDay() === 3) ? 53 : 52
  },
  getNumberOfWeeksInMonth (year, month) {
    const weeks = []
    const firstDate = new Date(year, month - 1, 1)
    const lastDate = new Date(year, month, 0) // get number of last day from month - 1 (28, 29, 30 or 31 ?)
    const numDays = lastDate.getDate()

    let dayOfWeekCounter = firstDate.getDay()

    for (let date = 1; date <= numDays; date++) {
      // below: 1 for Monday (by default first day = 0 is Sunday)
      if (dayOfWeekCounter === 1 || weeks.length === 0) {
        weeks.push([])
      }
      weeks[weeks.length - 1].push(date)
      dayOfWeekCounter = (dayOfWeekCounter + 1) % 7
    }

    // create weeks details
    const results = weeks
      .filter(w => !!w.length)
      .map(w => ({
        start: w[0],
        end: w[w.length - 1],
        dates: w
      }))

    const completResult = {
      weeks: results,
      numberOfWeeks: 0
    }

    // count weeks (1 week = more or equals than 4 days)
    results.forEach(result => {
      if (result.dates.length > 3) {
        completResult.numberOfWeeks = completResult.numberOfWeeks + 1
      }
    })

    return completResult
  }
}
